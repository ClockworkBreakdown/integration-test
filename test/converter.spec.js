const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
    describe("Hex to RGB conversion", () => {
        it("converts the basic colors", () => {
            const redRGB = converter.HexToRGB("ff0000");
            const greenRGB = converter.HexToRGB("00ff00");
            const blueRGB = converter.HexToRGB("0000ff");

            expect(redRGB).to.deep.equal([255,0,0]);
            expect(greenRGB).to.deep.equal([0,255,0]);
            expect(blueRGB).to.deep.equal([0,0,255]);
        });
    });
});
